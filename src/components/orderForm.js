import React from 'react'
import { Field, reduxForm } from 'redux-form';
import { Button, Form, FormGroup, Label } from 'reactstrap';

let OrderForm = props => {
  const { handleSubmit } = props;

  return (
    <Form onSubmit={handleSubmit}>
        <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Field name="email" component="input" type="email" className="form-control" />
        </FormGroup>
        <Button color="primary">Order now</Button>
    </Form>
  );
}

OrderForm = reduxForm({
  form: 'order'
})(OrderForm);

export default OrderForm;
