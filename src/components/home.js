import React, { PureComponent } from 'react';
import { Container, Row, Col } from 'reactstrap';

import ProductList from '../containers/productList';
import Cart from '../containers/cart';
import CartTotal from '../containers/cartTotal';

class Home extends PureComponent {
  render() {
    return (
        <Container className="App">
            <Row>
                <Col>
                    <ProductList />
                </Col>
                <Col>
                    <Cart />
                    <CartTotal />
                </Col>
            </Row>
        </Container>
    );
  }
}

export default Home;
