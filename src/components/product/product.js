import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListGroupItem } from 'reactstrap';

class Product extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.any.isRequired,
        productClicked: PropTypes.func.isRequired,
    }

    clicked = () => {
        this.props.productClicked(this.props.id);
    }

    render() {
        const { title, price } = this.props;

        return (
            <ListGroupItem action={true} onClick={this.clicked}>
                {title}
                <div className="float-right">
                    {price} PLN
                </div>
            </ListGroupItem>
        );
    }
}

export default Product;
