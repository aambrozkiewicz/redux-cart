import React, { PureComponent } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

import Cart from '../containers/cart';
import OrderForm from './orderForm';

class Checkout extends PureComponent {
    submit = values => {
        console.log(values);
    }

    render() {
        return (
            <Container className="App">
                <Row>
                    <Col>
                        <Link to="/">&larr; Go back</Link>
                        <hr />
                        <Cart disabled={true} />
                    </Col>
                    <Col>
                        <OrderForm onSubmit={this.submit} />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Checkout;
