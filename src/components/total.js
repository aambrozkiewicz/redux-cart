import React from 'react';
import { Link } from 'react-router-dom';

const Total = ({ total }) => {
    return (
        total ? 
        <div className="total" style={{display: total === 0 ? 'none': 'block'}}>
            Total <strong>{total} PLN</strong>
            &nbsp;
            <Link to="/checkout" className="btn btn-primary">Checkout</Link>
        </div>
        : 'Cart is empty.'
    );
}

export default Total;
