import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListGroupItem } from 'reactstrap';

class CartProduct extends Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        price: PropTypes.any.isRequired,
        quantity: PropTypes.number.isRequired,
        removeProduct: PropTypes.func.isRequired,
        disabled: PropTypes.bool,
    }

    clicked = () => {
        this.props.removeProduct(this.props.id);
    }

    render() {
        const { title, price, quantity, disabled } = this.props;

        return (
            <ListGroupItem action={true} onClick={this.clicked} disabled={disabled}>
                {quantity} &times; {title}
                <div className="float-right">
                    {price} PLN
                </div>
            </ListGroupItem>
        );
    }
}

export default CartProduct;
