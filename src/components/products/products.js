import React, { PureComponent } from 'react';
import Product from '../product/product';
import CartProduct from '../cartProduct/cartProduct';
import PropTypes from 'prop-types';
import { ListGroup } from 'reactstrap';

class Products extends PureComponent {
    componentWillMount() {
        this.props.fetchProducts();
    }

    render() {
        const { products, ...others } = this.props;

        return (
            <div className="products">
                <ListGroup>
                    {products.map((p, i) => {
                        return <Product {...p} {...others} key={i} />;
                    })}
                </ListGroup>
            </div>
        );
    }
}

export const CartProducts = ({ products, ...others }) => {        
    return (
        <div className="products">
            <ListGroup>
                {products.map((p, i) => {
                    return <CartProduct {...p} {...others} key={i} />;
                })}
            </ListGroup>
        </div>
    );
}

CartProducts.propTypes = {
    products: PropTypes.array,
}

export default Products;
