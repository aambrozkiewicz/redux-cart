import { createSelector } from 'reselect';

export const productsSelector = state => state.products.products;
export const cartSelector = state => state.products.cart;

export const productsByIdSelector = createSelector(
    productsSelector,
    products => products.reduce((prev, product) => {
        return {...prev, [product.id]: product};
    }, {})
);

export const cartContentsSelector = createSelector(
    cartSelector, productsByIdSelector,
    (cart, productsById) => {
        return Object.keys(cart).map((productId) => {
            return {
                ...productsById[productId],
                ...cart[productId],
            };
        });
    }
);

export const cartTotalSelector = createSelector(
    cartContentsSelector,
    cartContents => Math.round(cartContents.reduce((total, cartProduct) => (
        total + (cartProduct['price'] * cartProduct['quantity'])
    ), 0) * 100) / 100
)
