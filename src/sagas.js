import { call, put, takeLatest } from 'redux-saga/effects'
import { fetchProducts as apiFetchProducts } from './api';
import { Creators } from './reducers/products';

function* fetchProducts() {
   try {
        const response = yield call(apiFetchProducts)
        const products = yield response.json()
        yield put(Creators.productsFetchSucceeded(products))
   } catch (e) {
        yield put({type: "PRODUCTS_FETCH_FAILED", message: e.message});
   }
}

function* mySaga() {
    yield takeLatest("PRODUCTS_FETCH_REQUESTED", fetchProducts);
}

export default mySaga;
