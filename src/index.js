import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore, combineReducers } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import registerServiceWorker from './registerServiceWorker';
import { reducer as formReducer } from 'redux-form';
import mySaga from './sagas';

import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import App from './App';
import products from './reducers/products';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({
      products,
      form: formReducer,
  }),
  applyMiddleware(sagaMiddleware, logger)
);

sagaMiddleware.run(mySaga);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);

registerServiceWorker();
