import { createReducer, createActions } from 'reduxsauce';

export const INITIAL_STATE = {
    products: [],
    cart: {},
};

export const incCartQuantity = (state = INITIAL_STATE, { productId }) => {
    const cart = Object.assign({}, state.cart);
    const cartProduct = cart[productId] || {'quantity': 0};
    
    cartProduct['quantity'] += 1;
    cart[productId] = cartProduct;

    return { ...state, cart };
};

export const removeFromCart = (state = INITIAL_STATE, { productId }) => {
    const cart = Object.assign({}, state.cart);
    delete cart[productId];

    return { ...state, cart };
};

const productsFetchSucceeded = (state = INITIAL_STATE, { products }) => {
    return { ...state, products };
};

export const { Types, Creators } = createActions({
    productsFetchRequested: null,
    productsFetchSucceeded: ['products'],
    incCartQuantity: ['productId'],
    removeFromCart: ['productId'],
});

export const HANDLERS = {
    [Types.INC_CART_QUANTITY]: incCartQuantity,
    [Types.REMOVE_FROM_CART]: removeFromCart,
    [Types.PRODUCTS_FETCH_SUCCEEDED]: productsFetchSucceeded,
};

export default createReducer(INITIAL_STATE, HANDLERS);
