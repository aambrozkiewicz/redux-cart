import { connect } from "react-redux";
import Products from "../components/products/products";
import { Creators } from "../reducers/products";
import { bindActionCreators } from "redux";

const mapStateToProps = state => {
    return {
        products: state.products.products,
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchProducts: Creators.productsFetchRequested,
    productClicked: Creators.incCartQuantity,
}, dispatch);

const ProductList = connect(
    mapStateToProps,
    mapDispatchToProps
)(Products);

export default ProductList;
