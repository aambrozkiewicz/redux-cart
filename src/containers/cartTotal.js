import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { cartTotalSelector } from "../components/products/selectors";
import Total from "../components/total";

const mapStateToProps = createStructuredSelector({
    total: cartTotalSelector,
    visible: cartTotalSelector,
});

const CartTotal = connect(
    mapStateToProps,
)(Total);

export default CartTotal;
