import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { CartProducts } from "../components/products/products";
import { Creators } from "../reducers/products";
import { cartContentsSelector } from "../components/products/selectors";

const mapStateToProps = createStructuredSelector({
    products: cartContentsSelector,
});

const mapDispatchToProps = dispatch => {
    return {
        removeProduct: (productId) => dispatch(Creators.removeFromCart(productId)),
    };
};

const Cart = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartProducts);

export default Cart;
